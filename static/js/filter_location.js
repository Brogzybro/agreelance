$("#search-btn").click(function () {
    let search_loc_input = $("#search-input").val();
    let search_loc = search_loc_input.toLowerCase();
    $(".project").each(function () {
        let loc = $(this).attr("location");
        console.log(this);
        console.log(loc);
        loc = loc.toLowerCase();
        $(this).show();
        if (loc !== search_loc){
            $(this).hide();
        }
    });
    let filter_element = $("#current-filter");
    filter_element.text(search_loc_input + " X");
    filter_element.show();
    $("#search-input").val("");
});

$("#current-filter").click(function () {
   $(this).hide();
    $(".project").each(function () {
        $(this).show();
    })
});