$('#make-compact').click(function () {
    let isActive = $(this).val();
    if (isActive === "false"){
        $(this).val(true);
        $(this).addClass("active2");
        makeAllProjectsCompact();
    }else{
        $(this).val(false);
        $(this).removeClass("active2");
        makeAllProjectsBig();
    }
});

function makeAllProjectsCompact() {
    $('.big-format').each(function () {
        $(this).hide();
    });
    $('.compact-format').each(function () {
        $(this).show();
    })
}
function makeAllProjectsBig() {
    $('.compact-format').each(function () {
        $(this).hide();
    });
    $('.big-format').each(function () {
        $(this).show();
    })
}