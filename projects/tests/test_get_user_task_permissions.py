from django.contrib.auth.models import User
from django.test import TestCase

from projects.models import Task
from projects.views import task_get_user_task_permissions


class TestGetUserTaskPermissions(TestCase):

    fixtures = ['seed.json']

    def setUp(self):
        self.task = Task.objects.get(pk=1)
        self.project_owner = self.task.project.user.user

    def test_get_user_permissions_when_user_is_task_owner(self):

        permissions = task_get_user_task_permissions(self.project_owner, self.task)

        expected_permissions = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }

        self.assertEqual(permissions, expected_permissions)

    def test_get_user_permissions_for_accepted_task_offerer(self):

        offerer = User.objects.all().filter(username='joe')[0]
        permissions = task_get_user_task_permissions(offerer, self.task)

        expected_permissions = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }

        self.assertEqual(permissions, expected_permissions)

    def test_get_user_permission_for_user_who_is_not_owner(self):

        user = User.objects.all().filter(username='harrypotter')[0]

        permissions = task_get_user_task_permissions(user, self.task)

        expected_permissions = {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        }

        self.assertEqual(permissions, expected_permissions)
