from django.test import TestCase
from factory.fuzzy import FuzzyText, FuzzyInteger

from projects.forms import TaskOfferForm


class TestBoundaryValuesForProjectOffers(TestCase):
    def setUp(self):
        self.normal_title = FuzzyText(length=30)
        self.long_title = FuzzyText(length=200)
        self.too_long_title = FuzzyText(length=201)
        self.short_title = FuzzyText(length=1)
        self.no_title = FuzzyText(length=0)

        self.normal_description = FuzzyText(length=100)
        self.long_description = FuzzyText(length=500)
        self.too_long_description = FuzzyText(length=501)
        self.short_description = FuzzyText(length=1)
        self.no_description = FuzzyText(length=0)

        self.normal_price = FuzzyInteger(2, 999998)
        self.high_price = 999999
        self.too_high_price = 1000000
        self.low_price = 1
        self.zero_price = 0
        self.negative_price = FuzzyInteger(-999999, -1)

    def get_normal_values(self):
        values = {
            'title': self.normal_title.fuzz(),
            'description': self.normal_description.fuzz(),
            'price': self.normal_price.fuzz()
        }
        return values

    def test_normal_values(self):
        values = self.get_normal_values()
        form = TaskOfferForm(values)
        self.assertTrue(form.is_valid())

    def test_long_high_values(self):
        values = {
            'title': self.long_title.fuzz(),
            'description': self.long_description.fuzz(),
            'price': self.high_price
        }
        form = TaskOfferForm(values)
        self.assertTrue(form.is_valid())

    def test_short_low_values(self):
        values = {
            'title': self.short_title.fuzz(),
            'description': self.short_description.fuzz(),
            'price': self.low_price
        }
        form = TaskOfferForm(values)
        self.assertTrue(form.is_valid())

    def test_too_long_title(self):
        values = self.get_normal_values()
        values['title'] = self.too_long_title.fuzz()
        form = TaskOfferForm(values)
        self.assertFalse(form.is_valid())

    def test_too_long_description(self):
        values = self.get_normal_values()
        values['description'] = self.too_long_description.fuzz()
        form = TaskOfferForm(values)
        self.assertFalse(form.is_valid())

    def test_price_too_high(self):  # No there is no upper limit to price
        values = self.get_normal_values()
        values['price'] = self.too_high_price
        form = TaskOfferForm(values)
        self.assertFalse(not form.is_valid())

    def test_no_title(self):
        values = self.get_normal_values()
        values['title'] = self.no_title.fuzz()
        form = TaskOfferForm(values)
        self.assertFalse(form.is_valid())

    def test_no_description(self):
        values = self.get_normal_values()
        values['description'] = self.no_description.fuzz()
        form = TaskOfferForm(values)
        self.assertFalse(form.is_valid())

    def test_zero_price(self):
        values = self.get_normal_values()
        values['price'] = self.zero_price
        form = TaskOfferForm(values)
        self.assertTrue(form.is_valid())

    def test_negative_price(self):  # Should only accept positive numbers
        values = self.get_normal_values()
        values['price'] = self.negative_price.fuzz()
        form = TaskOfferForm(values)
        self.assertFalse(not form.is_valid())

