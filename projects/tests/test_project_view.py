from django.test import TestCase, Client
from django.urls import reverse
from projects.models import TaskOffer, Project


class TestProjectView(TestCase):

    fixtures = ['seed.json']

    def setUp(self):

        self.client = Client()
        self.client.login(username='joe', password='qwerty123')

        self.project_owner_client = Client()
        self.project_owner_client.login(username='admin', password='qwerty123')

    def test_project_view_offer_submit(self):
        project_id = 1
        title = "Test project title"
        description = "Test project description"
        price = 200
        task_value = 1

        response = self.client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'title': title,
                'description': description,
                'price': price,
                'taskvalue': task_value,
                'offer_submit': '',
            }
        )

        actual_task_offer = TaskOffer.objects.all().order_by('-id')[0]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(actual_task_offer.title, title)
        self.assertEqual(actual_task_offer.description, description)
        self.assertEqual(actual_task_offer.price, price)
        self.assertEqual(actual_task_offer.offerer, response.context['user'].profile)
        self.assertEqual(actual_task_offer.status, TaskOffer.PENDING)

    def test_project_view_offer_response(self):
        project_id = 1
        status = TaskOffer.ACCEPTED
        feedback = "Some test feedback"
        taskofferid = 1

        response = self.project_owner_client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'status': status,
                'feedback': feedback,
                'taskofferid': taskofferid,
                'offer_response': ''
            }
        )

        task_offer = TaskOffer.objects.get(pk=taskofferid)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(task_offer.status, status)
        self.assertEqual(task_offer.feedback, feedback)

    def test_project_view_status_change(self):
        status = Project.FINISHED
        project_id = 1

        response = self.project_owner_client.post(
            reverse('project_view', kwargs={'project_id': project_id}), {
                'status': status,
                'status_change': ''
            }
        )

        updated_project = Project.objects.get(pk=project_id)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated_project.status, status)
