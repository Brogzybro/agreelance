from django.http import HttpResponseRedirect
from user.models import Profile
from .models import Project, Task, TaskFile, TaskOffer, ProjectCategory
from .forms import ProjectForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.task_view import upload_file_to_task, get_user_task_permissions, task_view_, task_permissions_


def projects(request):
    projects = Project.objects.all()
    project_categories = ProjectCategory.objects.all()
    return render(request,
        'projects/projects.html',
        {
            'projects': projects,
            'project_categories': project_categories,
        }
    )

@login_required
def new_project(request):
    from django.contrib.sites.shortcuts import get_current_site
    current_site = get_current_site(request)
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.user = request.user.profile
            project.category =  get_object_or_404(ProjectCategory, id=request.POST.get('category_id'))
            project.save()

            send_email(current_site, project, request)

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')
            for i in range(0, len(task_title)):
                Task.objects.create(
                    title = task_title[i],
                    description = task_description[i],
                    budget = task_budget[i],
                    project = project,
                )
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def send_email(current_site, project, request):
    people = Profile.objects.filter(categories__id=project.category.id)
    from django.core import mail
    for person in people:
        if person.user.email:
            try:
                with mail.get_connection() as connection:
                    mail.EmailMessage(
                        "New Project: " + project.title,
                        "A new project you might be interested in was created and can be viwed at " + current_site.domain + '/projects/' + str(
                            project.id), "Agreelancer", [person.user.email],
                        connection=connection,
                    ).send()
            except Exception as e:
                from django.contrib import messages
                messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = 0

    for item in tasks:
        total_budget += item.budget

    if request.user == project.user.user:

        if request.method == 'POST' and 'offer_response' in request.POST:
            instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
            offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
            if offer_response_form.is_valid():
                offer_response = offer_response_form.save(commit=False)

                if offer_response.status == 'a':
                    offer_response.task.read.add(offer_response.offerer)
                    offer_response.task.write.add(offer_response.offerer)
                    project = offer_response.task.project
                    project.participants.add(offer_response.offerer)


                offer_response.save()
        offer_response_form = TaskOfferResponseForm()

        if request.method == 'POST' and 'status_change' in request.POST:
            status_form = ProjectStatusForm(request.POST)
            if status_form.is_valid():
                project_status = status_form.save(commit=False)
                project.status = project_status.status
                project.save()
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'status_form': status_form,
        'total_budget': total_budget,
        'offer_response_form': offer_response_form,
        })


    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            task_offer_form = TaskOfferForm(request.POST)
            if task_offer_form.is_valid():
                task_offer = task_offer_form.save(commit=False)
                task_offer.task =  Task.objects.get(pk=request.POST.get('taskvalue'))
                task_offer.offerer = request.user.profile
                task_offer.save()
        task_offer_form = TaskOfferForm()

        return render(request, 'projects/project_view.html', {
        'project': project,
        'tasks': tasks,
        'task_offer_form': task_offer_form,
        'total_budget': total_budget,
        })


@login_required
def task_upload_file_to_task(request, project_id, task_id):
    return upload_file_to_task(request, project_id, task_id)


def task_get_user_task_permissions(user, task):
    return get_user_task_permissions(user, task)


@login_required
def task_view(request, project_id, task_id):
    return task_view_(request, project_id, task_id)


@login_required
def task_permissions(request, project_id, task_id):
    return task_permissions_(request, project_id, task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
